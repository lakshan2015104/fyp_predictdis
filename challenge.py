import tweepy
import csv
import numpy as np
from textblob import TextBlob
from keras.models import Sequential
from keras.layers import Dense

import demo

#data collection
dates = []
prices = []
def get_data(filename):
	with open(filename, 'r') as csvfile:
		csvFileReader = csv.reader(csvfile)
		next(csvFileReader)
		for row in csvFileReader:
			dates.append(int(row[0].split('-')[0]))
			prices.append(float(row[1]))
	return

#Step 5 reference your CSV file here
get_data('your_company_stock_data.csv')

#Step 6 In this function, build your neural network model using Keras, train it, then have it predict the price
#on a given day. We'll later print the price out to terminal.
def predict_prices(dates, prices, x):

	predicted_price = demo.predict_price(dates, prices, 29)
	print(predicted_price)


def main():
	get_data('emg_all_features_labeled')