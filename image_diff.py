# import the necessary packages
from skimage.measure import compare_ssim
import argparse
import imutils
import cv2


def avg_score(input_grey, type):

    count = 0
    if type == 'healthy':
        count = 7
        img_path = 'images/emg_healthy_'

    if type == 'myopathy':
        count = 7
        img_path = 'images/emg_myopathy_'

    if type == 'neuropathy':
        count = 7
        img_path = 'images/emg_neuropathy_'


    total_score  = 0

    for i in range(count):
        if i == 0:
            continue

        data_image = img_path +  str(i) + '.png'

        print(data_image)


        imageA = cv2.imread(data_image)


        # convert the images to grayscale
        grey = cv2.cvtColor(imageA, cv2.COLOR_BGR2GRAY)

        (score, diff) = compare_ssim(grey, input_grey, full=True)
        print(score)
        total_score += score

    avg = total_score / (count-1)

    print('total ', type, ' : ', total_score)

    print('average ', type, ' : ', avg)

    return avg




def load_image(png_path):
    input_image = png_path;

     # construct the argument parse and parse the arguments

    # ap = argparse.ArgumentParser()
    # ap.add_argument("-f", "--first", required=True,
    #             help="first input image")
    # ap.add_argument("-s", "--second", required=True,
    #             help="second input image")
    # ap.add_argument("-t", "--third", required=True,
    #             help="third input image")
    # ap.add_argument("-i", "--input", required=True,
    #             help="input image")
    # args = vars(ap.parse_args())

    healthy_image ='images/emg_healthy.png';
    neuropathy_image ='images/emg_neuropathy.png';
    myopathy_image ='images/emg_myopathy.png';

    imageA = cv2.imread(healthy_image)
    imageB = cv2.imread(neuropathy_image)
    imageC = cv2.imread(myopathy_image)
    imageD = cv2.imread(input_image)

    # convert the images to grayscale
    grayA = cv2.cvtColor(imageA, cv2.COLOR_BGR2GRAY)
    grayB = cv2.cvtColor(imageB, cv2.COLOR_BGR2GRAY)
    grayC = cv2.cvtColor(imageC, cv2.COLOR_BGR2GRAY)
    #grayD = cv2.cvtColor(imageD, cv2.COLOR_BGR2GRAY)

    # canny edge detection, crop the image and resize the image
    edged = cv2.Canny(imageD, 10, 250)
    (cnts, _) = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    idx = 0
    for c in cnts:
        x, y, w, h = cv2.boundingRect(c)
        if w > 50 and h > 50:
            idx += 1
            new_img = imageD[y:y + h, x:x + w]
            # cv2.imwrite(str(idx) + '.png', new_img)
    resized_image = cv2.resize(new_img, (800, 600))
    cv2.imwrite('images/processed.png', resized_image)

    grayD = cv2.cvtColor(resized_image, cv2.COLOR_BGR2GRAY)

    # compute the Structural Similarity Index (SSIM) between the two images, ensuring that the difference image is returned
    (score1, diff) = compare_ssim(grayA, grayD, full=True)
    (score2, diff) = compare_ssim(grayB, grayD, full=True)
    (score3, diff) = compare_ssim(grayC, grayD, full=True)
    diff = (diff * 255).astype("uint8")
    # print("SSIM A: {}".format(score1))
    # print("SSIM B: {}".format(score2))
    # print("SSIM C: {}".format(score3))

    print('normal healthy : ',score1)
    print('normal myopathy : ', score3)
    print('normal neuropathy : ', score2)

    result=''

    score1 = avg_score(grayD, 'healthy')
    score2 = avg_score(grayD, 'myopathy')
    score3 = avg_score(grayD, 'neuropathy')

    if (score1 >= score2) and (score1 >= score3) and (score1 >= 0.6):
        largest = score1
        result = 'You are healthy'
        print("You are healthy")
    elif (score2 >= score1) and (score2 >= score3) and (score2 >= 0.6):
        largest = score2
        result = "You are in a risk of myopathy disease"
        print("You are in a risk of myopathy disease")
    elif (score3 >= score1) and (score3 >= score2) and (score3 >= 0.6):
        largest = score3
        result = "You are in a risk of neuropathy disease"
        print("You are in a risk of neuropathy disease")
    else:
        largest = score3
        result = "Input error. Please upload a suitable EMG graph image"
        print("Input error. Please upload a suitable EMG graph image")

    # print("The largest number between",score1,",",score2,"and",score3,"is",largest)

    thresh = cv2.threshold(diff, 0, 255,
	    cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]
    cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
	    cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)

    # loop over the contours
    for c in cnts:
        # images differ
        (x, y, w, h) = cv2.boundingRect(c)
        cv2.rectangle(imageA, (x, y), (x + w, y + h), (0, 0, 255), 2)
        cv2.rectangle(imageD, (x, y), (x + w, y + h), (0, 0, 255), 2)

    # show the output images
    # cv2.imshow("Original", imageA)
    # cv2.imshow("Modified", imageD)
    # cv2.imshow("Diff", diff)
    # cv2.imshow("Thresh", thresh)
    # cv2.waitKey(0)

    return result