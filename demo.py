import cv2
import csv
import numpy as np
from sklearn.svm import SVR
import matplotlib.pyplot as plt

import image_diff

# plt.switch_backend('newbackend')


def get_data(filename):
    time = []
    amplitude = []
    with open(filename, 'r') as csvfile:
        csvFileReader = csv.reader(csvfile)
        next(csvFileReader)  # skipping column names
        for row in csvFileReader:
            time.append(float(row[0]))
            amplitude.append(float(row[1]))

    predict_price(filename, time, amplitude, 1)


# return

def predict_price(filename, dates, prices, x):
    dates = np.reshape(dates, (len(dates), 1))  # converting to matrix of n X 1

    svr_lin = SVR(kernel='linear', C=1e3)
    svr_poly = SVR(kernel='poly', C=1e3, degree=2)
    svr_rbf = SVR(kernel='rbf', C=1e3, gamma=0.1)  # defining the support vector regression models
    # svr_rbf.fit(dates, prices) # fitting the data points in the models
    # svr_lin.fit(dates, prices)
    # vr_poly.fit(dates, prices)

    plt.plot(dates, prices)  # plotting the initial datapoints
    # plt.plot(dates, svr_rbf.predict(dates), color= 'red', label= 'RBF model') # plotting the line made by the RBF kernel
    # plt.plot(dates,svr_lin.predict(dates), color= 'green', label= 'Linear model') # plotting the line made by linear kernel
    # plt.plot(dates,svr_poly.predict(dates), color= 'blue', label= 'Polynomial model') # plotting the line made by polynomial kernel
    plt.xlabel('Date')
    plt.ylabel('Price')
    plt.title('Support Vector Regression')
    plt.legend()
    ax = plt.gca()

    filename = filename.split('.')
    filename = filename[0]
    if (filename != 'emg_neuropathy'):
        ax.set_ylim([-0.7, 0.7])  # set length of the graph y axis
        ax.set_xlim([1, 1.5])  # set length of the graph x axis
    else:
        ax.set_ylim([-3, 1])  # set length of the graph y axis
        ax.set_xlim([1, 1.5])  # set length of the graph x axis

    plt.savefig(filename + '.png', dpi=100)
    plt.show()


# return svr_rbf.predict(x)[0], svr_lin.predict(x)[0], svr_poly.predict(x)[0]

def generate_healthy_graph():
    get_data('emg_healthy.csv')


def generate_neuropathy_graph():
    get_data('emg_neuropathy.csv')


def generate_myopathy_graph():
    get_data('emg_myopathy.csv')

def convert_img_to_graph():
    img = cv2.imread('sample.jpeg', 0)
    color = ('b', 'g', 'r')
    for i, col in enumerate(color):
        histr = cv2.calcHist([img], [i], None, [256], [0, 256])
        plt.plot(histr, color=col)
        plt.xlim([0, 256])
    plt.show()

# get_data('emg_myopathy.csv') # calling get_data method by passing the csv file to it
# print "Dates- ", dates
# print "Prices- ", prices

generate_healthy_graph()
generate_neuropathy_graph()
generate_myopathy_graph()
convert_img_to_graph()

# predicted_price = predict_price(time, amplitude, 1)
