from flask import Flask, request, jsonify
import base64
import uuid
from flask_cors import CORS, cross_origin
from image_diff import load_image

app = Flask(__name__)
CORS(app, support_credentials=True)


@app.route('/', methods=['POST'])
@cross_origin(supports_credentials=True)
def home():
    return "Home welcome manager!"

@app.route('/upload', methods=['POST'])
@cross_origin(supports_credentials=True)
def upload_image():
    image = base64.b64decode(request.json['image'])
    filename = 'UploadImages/'+str(uuid.uuid4().hex)+".png"
    print(filename)
    with open(filename, 'wb') as f:
        f.write(image)

    result = load_image(filename)
    return jsonify({'status': 'success', 'message': 'Disease Identified', 'data': result}), 201
    #return "Sucess!"


if __name__ == "__main__":
    app.run(debug=True)